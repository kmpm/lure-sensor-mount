//Copyright 2015 Peter Magnusson
include <./mount-meta.scad>

module cone(height) {
	cylinder(h = height, d1 = PLUG_MIN, d2 = PLUG_MAX, center = false); 
}

module wing(height=30, thickness=3, radius=7, base_width=12) {

	tilt=base_width-radius*2;
	pheigh = height - radius * 2  + radius / 3;

	tA = thickness / 2;

		union() {
			translate([0, -tA, height - radius]){ 
				rotate([-90,90,0]){
					cylinder(h=thickness, r=radius);
				}
			}

			translate([-radius, -tA, pheigh]) {
				cube([radius * 2 , thickness ,height - pheigh - radius]);
			}

			
			polyhedron(
			 	points=[ 
					[base_width,tA,0],[base_width, -tA ,0],
					[-base_width, -tA ,0], [-base_width, tA, 0], // the four points at base
					[radius, tA, pheigh],[radius,-tA,pheigh],
					[-radius,-tA,pheigh],[-radius,tA,pheigh], //four point at top
				],
				faces=[ 
					[0,1,5,4],// x+
					[4,5,6, 7], //[6, 7, 4], // top
					[2, 3, 7, 6], // x-
					[3, 0, 4, 7],// y+
					[1, 2, 6, 5], //[1, 2, 6],// Y-
					[1,0,3,2] // bottom
				] 
			);
		}
}

module bolt(height, nut_size) {
	translate([-nut_size,0,-1]){
		cylinder(h=BOTTOM_HEIGHT+2, r=nut_size*0.5);
	}
}


module mount (thickness=3, height=40, separation=10, base_width=10) {
	translate([1, -separation - thickness/2, 0]) {
		wing(thickness=thickness, height=height, base_width=base_width);
	}
	translate([1, separation + thickness/2, 0]) {
		wing(thickness=thickness, height=height, base_width=base_width);
	}	
}

module composit(height, nut_size) {
    difference() {
		cone(height=height);
		bolt(nut_size=nut_size);
		slit(height);
			
	};    
}

module plugg(height=BOTTOM_HEIGHT, nut_size=NUT_SIZE, mount_height=MOUNT_HEIGHT, offset=-4) {
	translate([offset, 0, height]) {
        mount(height=mount_height);
    }
    difference() {
        composit(height=height, nut_size=nut_size);
        translate([-nut_size, 0, height-3.5]) {
            nutHole(5);
        }
        
    }
}

plugg();

