//Copyright 2015 Peter Magnusson

include <./mount-meta.scad>

PLUG_MAX=35;

use <MCAD/nuts_and_bolts.scad>

module cone() {
	cylinder(h = SHALLOW_TOP_HEIGHT, d1 = PLUG_MIN, d2 = PLUG_MAX, center = false);
	translate([0,0,SHALLOW_TOP_HEIGHT-CAP]) {
		cylinder(h = CAP, r=CAP_DIA/2 );
	}
}



module bolt(nut_size=NUT_SIZE) {
	translate([-nut_size,0,-1]){
		cylinder(h=SHALLOW_TOP_HEIGHT+CAP, r=nut_size*0.5);
    
	}
    depth=4;
    //countersink
    translate([-nut_size,0,SHALLOW_TOP_HEIGHT-depth]){
        //nutHole(NUT_SIZE);
            cylinder(h=depth+1, r=nut_size*1.5);
    }	
}



module support(nut_size=NUT_SIZE, depth=4) {
    color([1,0.5,0,1]) {
        translate([-nut_size, 0, SHALLOW_TOP_HEIGHT-depth]) {
            funnel(h=depth, r2=nut_size*0.6, r1=nut_size*1.3, r=nut_size*0.5);
            //ring(h=depth, r=nut_size);
            
        }
    }
}

module plugg() {
    
	difference() {
		cone();
		bolt();
		slit(SHALLOW_TOP_HEIGHT);
  
	}
    support();
    
}

plugg();

