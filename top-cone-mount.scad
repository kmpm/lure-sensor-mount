//Copyright 2015 Peter Magnusson
include <./mount-meta.scad>


module cone(height) {
	cylinder(h = height, d1 = PLUG_MIN, d2 = PLUG_MAX, center = false); 
}


module bolt(height) {
	translate([-NUT_SIZE,0,-1]){
		cylinder(h=height+2, r=NUT_SIZE*0.5);
	}
}


module plugg(height) {
	
	difference() {
		cone(height);
		bolt(height);
		slit(height);
			
	}
}


plugg(height=TOP_HEIGHT);