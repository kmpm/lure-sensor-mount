//Copyright 2015 Peter Magnusson
use <MCAD/nuts_and_bolts.scad>

$fn=100;
PLUG_MIN=34;
PLUG_MAX=39.5;

CABLE_DIA=7.2;
NUT_SIZE=6;
CAP=2;
CAP_DIA=PLUG_MAX + 10;
MOUNT_WIDTH=10;
MOUNT_HEIGHT=35;
BOTTOM_HEIGHT=15;
TOP_HEIGHT=17;
SHALLOW_TOP_HEIGHT=10;



module slit(height, nut_size=NUT_SIZE, cable_dia=CABLE_DIA) {
    translate([nut_size+3, 0, -0.5]){
        cylinder(h=height+1, r=cable_dia/2);
        
        translate([0,-cable_dia/2,0]){
            cube([15,cable_dia,21]);
        }
    }
}


module ring(h, r, t=1) {
    difference() {
        cylinder(h=h, r=r);
        translate([0, 0, -0.5]) {
            cylinder(h=h+1, r=r-t);
        }
    }
}

module funnel(h, r1, r2, r=2) {
    
    difference() {
        cylinder(h=h, r1=r1, r2=r2);
        translate([0, 0, -0.5]) {
            cylinder(h=h+1, r=r);
        }
    }
}